#!/usr/bin/python
###########################################################################
#    Copyright (C) 2007 by Andrew Mahone
#    <andrew.mahone@gmail.com>
#
# Copyright: See COPYING file that comes with this distribution
#
###########################################################################
"""database objects for managing comics and users"""

from sqlobject import SQLObject, IntCol, BLOBCol, DateTimeCol, StringCol, \
    UnicodeCol, SQLMultipleJoin, ForeignKey, DatabaseIndex, sqlhub, connectionForURI
from sqlobject.events import listen, RowUpdateSignal, RowCreateSignal
from NameEnc import Encode, Decode
import threading, struct, base64, sha, hmac, random
from calendar import timegm
CommitLock = threading.Lock()

_b64_alphabet = \
    tuple("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_")
_b64_reverse = dict(zip(_b64_alphabet, range(64)))

def DigestPassword(password, key=None):
    if key == None:
        if random._urandom:
            key = random._urandom(20)
        else:
            random.seed()
            key = ''.join(chr(random.randbits(8)) for i in range(20))
    ret = hmac.new(key, password, sha).digest() + key
    return ret

def _intToWebID(num):
    nums = []
    nums.append(num & 63)
    num >>=6
    while num and num != -1:
        nums.append (num & 63)
        num >>=6
    if num == -1 and not (nums[-1] & 32):
        nums.append(63)
    elif not num and (nums[-1] & 32):
        nums.append(0)
    return ''.join([_b64_alphabet[n] for n in reversed(nums)])

def _webIDToInt(webid):
    nums = [_b64_reverse[n] for n in webid]
    if nums[0] & 32:
        nums[0] = 64 - nums[0]
        sign = -1
    else:
       sign = 1
    return sign * reduce(lambda x,y: (x << 6) + y, nums)

class Comic(SQLObject):
    class sqlmeta:
        defaultOrder = 'name'
    def _init(self, *args, **kw):
        self._baseSerial = None
        SQLObject._init(self, *args, **kw)
    updated = DateTimeCol(default=DateTimeCol.now)
    name = UnicodeCol(alternateID='true')
    issues = SQLMultipleJoin('Issue')
    images = SQLMultipleJoin('Image')
    nameIndex = DatabaseIndex('name')
    dirname = property(lambda self: Encode(self.name))
    def _get_webID(self):
        return _intToWebID(self.id)
    def _get_webTS(self):
        return _intToWebID(timegm(self.updated.utctimetuple()))
    def _get_baseSerial(self):
        if self._baseSerial is None and self.issues.count():
            self._baseSerial = self.issues.limit(1)[0].serial
        return self._baseSerial
    @classmethod
    def byDirname(cls, name):
        return cls.byName(Decode(name))
    @classmethod
    def byWebID(cls, webid):
        return cls.get(_webIDToInt(webid))
    def UpdateTimeStamp(self):
        self.updated = DateTimeCol.now()

class Issue(SQLObject):
    class sqlmeta:
        defaultOrder = 'serial'
    title = UnicodeCol()
    url = UnicodeCol()
    serial = IntCol()
    comic = ForeignKey('Comic', cascade=True)
    images = SQLMultipleJoin('Image')
    comicIndex = DatabaseIndex('comic')
    serialIndex = DatabaseIndex('serial')
    serialComicIndex = DatabaseIndex('serial', 'comic', unique=True)
    def _get_issueNum(self):
        return self.serial + 1 - self.comic.baseSerial
    def _get_webID(self):
        return _intToWebID(self.id)
    @classmethod
    def byWebID(cls, webid):
        return cls.get(_webIDToInt(webid))

class Image(SQLObject):
    class sqlmeta:
        defaultOrder = 'serial'
    title = UnicodeCol()
    extra = UnicodeCol()
    url = UnicodeCol()
    file = UnicodeCol()
    mime = StringCol()
    serial = IntCol()
    comic = ForeignKey('Comic', cascade=True)
    issue = ForeignKey('Issue', cascade=True)
    issueIndex = DatabaseIndex('issue')
    comicIndex = DatabaseIndex('comic')
    def _get_webID(self):
        return _intToWebID(self.id)
    @classmethod
    def byWebID(cls, webid):
        return cls.get(_webIDToInt(webid))

class User(SQLObject):
    class sqlmeta:
        table = 'user_tbl'
    name = StringCol(alternateID=True)
    password = BLOBCol()
    comics = SQLMultipleJoin('UserComic', joinColumn='user_id')
    def _get_webID(self):
        return _intToWebID(self.id)
    @classmethod
    def byWebID(cls, webid):
        return cls.get(_webIDToInt(webid))
    def hasComic(self, comic):
        if isinstance(comic, basestring):
            comic = _webIDToInt(comic)
        elif hasattr(comic, 'id'):
            comic = comic.id
        return UserComic.selectBy(comicID=comic, userID=self.id).count() > 0

    def getComic(self, comic):
        if isinstance(comic, basestring):
            comic = _webIDToInt(comic)
        elif hasattr(comic, 'comic'):
            comic = comic.comic.id
        elif hasattr(comic, 'id'):
            comic = comic.id
        sel = UserComic.selectBy(comicID=comic, userID=self.id)
        if sel.count():
            return sel[0]
        else:
            return None
    def _set_password(self, password):
        self._SO_set_password(DigestPassword(password))
    def testPassword(self, password):
        return self.password == DigestPassword(password, self.password[20:])

class UserComic(SQLObject):
    user = ForeignKey('User', cascade=True)
    comic = ForeignKey('Comic', cascade=True)
    lastissue = ForeignKey('Issue', default=None, cascade='null')
    updated = property(lambda self: self.comic.updated)
    name = property(lambda self: self.comic.name)
    issues = property(lambda self: self.comic.issues)
    images = property(lambda self: self.comic.images)
    dirname = property(lambda self: self.comic.dirname)
    def _get_webID(self):
        return _intToWebID(self.id)
    @classmethod
    def byWebID(cls, webid):
        return cls.get(_webIDToInt(webid))
    def _set_lastissue(self, issue):
        if isinstance(issue, basestring):
            issue = Issue.byWebID(issue)
        elif isinstance(issue, int):
            issue = Issue.get(issue)
        if issue is None:
            self._SO_set_lastissue(None)
            return
        elif hasattr(issue, 'comic') and hasattr(issue.comic, 'id'):
            if issue.comic.id != self.comic.id:
                return
            else:
                self._SO_set_lastissue(issue)


def SetThreadConnection():
    from Comic import Conf
    if not hasattr(sqlhub, 'threadConnection'):
        sqlhub.threadConnection = connectionForURI(Conf.comic_db)

def DoInTransaction(func, *args, **kwargs):
    try:
        oldConn = sqlhub.threadConnection
        oldConnThreaded = True
    except AttributeError:
        oldConn = sqlhub.processConnection
        oldConnThreaded = False
# There seems to be no way to make this work with transactions in sqlite. In
# any case, threaded fetches can only be interrupted during network fetch, or
# by an exception. Network fetches never happen between DB operations which
# would be grouped in a transaction, anyway, and we'll just have to hope that
# DB stores will work robustly enough not to cause trouble.
    if oldConn.dbName == 'sqlite':
        return func(*args, **kwargs)
    conn = oldConn.transaction()
    if oldConnThreaded:
        sqlhub.threadConnection = conn
    else:
        sqlhub.processConnection = conn
    try:
        try:
            ret = func(*args, **kwargs)
        except:
            conn.rollback()
            raise
        else:
            conn.commit(close=True)
    finally:
        if oldConnThreaded:
            sqlhub.threadConnection = oldConn
        else:
            sqlhub.processConnection = oldConn

def _IssueUpdateParentTime(issue):
    issue.comic.UpdateTimeStamp()
    issue.comic._baseserial = None

def _IssueCreateListener(kwargs, post_funcs):
    post_funcs.append(_IssueUpdateParentTime)

def _IssueUpdatedListener(issue, kwargs):
    _IssueUpdateParentTime(issue)

listen(_IssueCreateListener, Issue, RowCreateSignal)
listen(_IssueUpdatedListener, Issue, RowUpdateSignal)

__all__ = [
    'Comic',
    'Issue',
    'Image'
]