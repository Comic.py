#!/usr/bin/python
###########################################################################
#    Copyright (C) 2007 by Andrew Mahone
#    <andrew.mahone@gmail.com>
#
# Copyright: See COPYING file that comes with this distribution
#
###########################################################################
'''COMIC SHOW!'s web-based viewer'''
import sqlobject, hmac, sha, random, sys, os, os.path, base64
from calendar import timegm
# I don't want stdout redirect, I can do output myself and it's not nice when
# I try to debug interactively
origstdout = sys.stdout
import web
sys.stdout = origstdout
import re, Comic
import mimetypes
from pkg_resources import resource_stream
from Comic import DB, Conf, Session

urls = (
    '/xml$', 'xml',
    '/xml2$', 'xml2',
    '/image$', 'image',
    '/login$', 'login',
    '/logout$', 'logout',
    '/password$', 'password',
    '/manageusers$', 'manageusers',
    '/s/(.*)', 'static',
    '/(?:index)?$', 'index',
)

def attr(name, value, filter=None):
    if value:
        if filter is True:
            value = web.websafe(value)
        elif filter:
            value = filter(value)
        return ' %s="%s"' % (name,value) 
    else:
        return ''

def ifc(condition, text, alttext=''):
    if condition:
        return text 
    else:
        return alttext

def acceptxml(func):
    def proxyfunc(*args, **kw):
        r = func(*args, **kw)
        accepted = web.ctx.environ['HTTP_ACCEPT']
        if 'application/xhtml+xml' in accepted:
            web.header("Content-Type","application/xhtml+xml; charset=utf-8")
            yield xml_prolog
        if isinstance(r, basestring):
            yield r
        elif isinstance(r, web.template.TemplateResult):
            yield r.__body__
        else:
            for i in r:
                yield i
    return proxyfunc

def dbsetup(func):
    def proxyfunc(*args, **kw):
        DB.SetThreadConnection()
        return func(*args, **kw)
    return proxyfunc

def Redirect(url):
    if 'homeurl' in Conf:
        url = urlparse.urljoin(Conf.homeurl,url)
    web.redirect(url)

def xml_escape(text):
    return re.sub("&|<|>|'|\"", lambda x: {'"':'&#34;','&':'&amp;',"'":'&#39;','<':'&lt;','>':'&gt;'}[x.group()], text)

def xml_enquote(text):
    text = xml_escape(text)
    apos_count = text.count("\'")
    quot_count = text.count("\"")
    if apos_count < quot_count:
        return "\'%s\'" % (text.replace("\'","&apos;"))
    else:
        return "\"%s\"" % (text.replace("\"","&quot;"))

class login:
    @Session.CheckAccess()
    @acceptxml
    def GET(self):
        r = render.login()
        return r

    @Session.CheckAccess()
    @acceptxml
    @dbsetup
    def POST(self):
        inp = web.input(username='', password='')
        try:
            user = DB.User.byName(inp.username)
        except sqlobject.SQLObjectNotFound:
            user = None
        if user and user.testPassword(inp.password):
            Session.DoLogin(user)
            Redirect('index')
        else:
            return render.login()

class logout:
    @Session.CheckAccess()
    @acceptxml
    @dbsetup
    def GET(self):
        Session.DoLogout()
        Redirect('index')

class manageusers:
    @Session.CheckAccess(auth=True,admin=True,onfail=lambda: Redirect('login'))
    @acceptxml
    @dbsetup
    def GET(self,session):
        users = DB.User.select()
        return render.manageusers(tuple(users))

    @Session.CheckAccess(auth=True,admin=True,onfail=lambda: Redirect('login'))
    @acceptxml
    @dbsetup
    def POST(self,session):
        i = web.input(user='', delete=False, add=False, changepassword=False, password='', verifypassword=None, newuser='')
        if i.changepassword:
            return render.password(i.user,i.user != 'admin','')
        elif i.delete:
            try:
                user = DB.User.byName(i.user)
            except sqlobject.SQLObjectNotFound:
                user = None
            if user and user.name != 'admin':
                DB.UserComic.deleteMany(where=(DB.UserComic.q.userID == user.id))
                user.destroySelf()
                return render.deleteusersuccess(i.user)
            else:
                return render.manageusers(tuple(User.select()))
        elif i.add:
            try:
                user = DB.User.byName(i.newuser)
            except sqlobject.SQLObjectNotFound:
                user = None
            if user:
                return render.adduser('user already exists: %s'%i.newuser),i.newuser
            elif not i.newuser:
                return render.adduser('','')
            elif i.user and not i.password:
                return render.adduser('please enter a password for the new user',i.newuser)
            elif i.user and not i.password == i.verifypassword:
                return render.adduser('passwords do not match',i.newuser)
            else:
                u = DB.User(name=i.newuser,password=i.password)
                return render.addusersuccess(i.newuser)

class password:
    @Session.CheckAccess(auth=True,onfail=lambda: Redirect('login'))
    @acceptxml
    @dbsetup
    def GET(self,session):
        return render.password(session['username'],False,'')

    @Session.CheckAccess(auth=True,onfail=lambda: Redirect('login'))
    @acceptxml
    @dbsetup
    def POST(self,session):
        i = web.input(oldpassword='',newpassword='',verifypassword=None,user=None)
        if i.user and  session['username'] == 'admin' and i.user != 'admin':
            username = i.user
            admin = True
        else:
            username = session['username']
            admin = False
        try:
            user = DB.User.byName(username)
        except sqlobject.SQLObjectNotFound:
            user = None
        if not user:
            return render.password(username,admin,'user does not exist')
        elif not i.newpassword and admin:
            return render.password(username,admin,'')
        elif not i.newpassword:
            return render.password(username,admin,'please enter a new password')
        elif not admin and not user.TestPassword(i.oldpassword):
            return render.password(username,admin,'invalid password, try again')
        elif i.newpassword != i.verifypassword:
            return render.password(username,admin,'the passwords do not match, try again')
        else:
            user.setPassword = i.newpassword
            return render.passwordsuccess(username,admin)

class image:
    @Session.CheckAccess(auth=True,onfail=lambda: Redirect('login'))
    @dbsetup
    def GET(self,session):
        i = web.input(id='')
        try:
            img = DB.Image.byWebID(i.id)
        except:
            raise
            web.notfound()
            return
        path = os.path.join(Conf.comic_store, img.comic.dirname, img.file)
        if not os.path.isfile(path):
            web.notfound()
            return
        if img.mime:
            web.header("Content-Type",img.mime)
        return open(path,'rb').read()

def comicssetup(session):
    i = web.input(comics_ts='',issues_ts='',comic='',issue='')
    try:
        user = DB.User.byName(session['username'])
    except:
        user = None
    comics = DB.Comic.select()
    ch = '||'.join([ '%x::%x' % (c.id, timegm(c.updated.utctimetuple())) for c in comics])
    ch = base64.urlsafe_b64encode(sha.sha(ch).digest())[:27]
    if i.comic:
        try:
            comic = DB.Comic.byWebID(i.comic)
        except:
            comic = None
    else:
        comic = None
    if comic:
        if i.issue:
            try:
                issue = DB.Issue.byWebID(i.issue)
            except:
                issue = None
                i.issue = ''
            if issue and issue.comic != comic:
                issue = None
                i.issue = ''
        else:
            issue = None
        if not issue:
            try:
                if user.hasComic(comic) and user.getComic(comic).lastissue:
                    issue = user.getComic(comic).lastissue
                elif comic.issues.count():
                    issue = comic.issues.reversed().limit(1)[0]
                else:
                    issue = None
            except: issue = None
        if i.issues_ts == comic.webTS:
            if issue:
                issues = (issue,)
            else:
                issues = ()
            issues_ts = ''
        else:
            issues_ts = comic.webTS
            issues = comic.issues
    else:
        issue = None
        issues_ts = ''
        issues = ()
    if i.comics_ts == ch:
        if comic:
            comics = (comic,)
        else:
            comics = ()
        comics_hash = ''
    else:
        comics_hash = ch
    if comic and user and issue and issue.webID == i.issue:
        uc = user.getComic(comic)
        uc.lastissue = issue
    return user, comics, comics_hash, comic, issues, issues_ts, issue

class index:
    @Session.CheckAccess(auth=True,onfail=lambda: Redirect('login'))
    @dbsetup
    @acceptxml
    def GET(self, session):
        user, comics, comics_hash, comic, issues, issues_ts, issue = comicssetup(session)
        return render.index(user, comics, comics_hash, comic, issues, issues_ts, issue)

class xml:
    @Session.CheckAccess(auth=True,onfail=lambda: Redirect('login'))
    @dbsetup
    def GET(self,session):
        web.header("Content-Type","application/xml; charset=utf-8")
        user, comics, comics_hash, comic, issues, issues_ts, issue = comicssetup(session)
        return render.xmlreq(user, comics, comics_hash, comic, issues, issues_ts, issue)

comics = []
comics_index = {}
comics_ts = 0
issues = []
web.webapi.internalerror = web.debugerror
xml_prolog = '<?xml version="1.0" encoding="UTF-8"?>\n'

class static:
    def GET(self, spath):
        if re.search(r'(?:^|/)\.\.(?:/|$)', spath):
            web.notfound()
            return
        s = resource_stream('Comic', 'data/static/' + spath)
        type = mimetypes.guess_type(spath)[0]
        if type:
            web.header("Content-Type", type + '; charset=utf-8')
        while True:
            dat = s.read(1024)
            if dat:
                yield dat
            else:
                break

class myrender(web.template.render):
    def _lookup(self, name):
        if re.search(r'(?:^|/)\.\.(?:/|$)', name):
            raise IOError("invalid path")
        try:
            stream = resource_stream('Comic', 'data/templates/' + name)
        except IOError:
            return web.template.render._lookup(self, name)
        return 'resource', ('Comic', 'data/templates/' + name)

    def _load_template(self, name):
        kind, data = self._lookup(name)
        if kind == 'resource':
            return web.template.Template(resource_stream(*data).read(), filename=name, **self._keywords)
        if kind == 'dir':
            return web.template.Render(data, cache=self._cache is not None, base=self._base, **self._keywords)
        elif kind == 'file':
            return web.template.Template(open(data).read(), filename=path, **self._keywords)
        else:
            raise AttributeError, "No template named " + name

DB.SetThreadConnection()
if not DB.User.selectBy(name='admin').count():
    DB.User(name='admin', password='admin')
# is there a way to specify the path of the /static/ dir for web.py?
web.template.Template.globals['tuple'] = tuple
web.template.Template.globals['attr'] = attr
web.template.Template.globals['ifc'] = ifc
render = myrender()
app = web.application(urls, globals())

def run():
    app.run(Session.WebSessionMiddleware)

if __name__ == "__main__":
    web.config.debug = True
    run()
