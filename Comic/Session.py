#!/usr/bin/python
###########################################################################
#    Copyright (C) 2007 by Andrew Mahone, Blaise Laflamme
#    <andrew.mahone@gmail.com>
#    <blaise@laflamme.org>
#
# Copyright: See COPYING file that comes with this distribution
#
###########################################################################
try:
    from beaker.middleware import SessionMiddleware
except ImportError:
    from beaker.session import SessionMiddleware
import sys, os.path
import Comic
from Comic import Conf, DB
# I don't want stdout redirect, I can do output myself and it's not nice when
# I try to debug interactively
origstdout = sys.stdout
import web
sys.stdout = origstdout

def DoLogin(user):
    session = web.ctx.environ['beaker.session']
    session['id'] = user.id
    session['username'] = user.name
    session['loggedin'] = 1
    session.save()

def DoLogout():
    web.ctx.environ['beaker.session'].invalidate()

def CheckAuth(session):
    return (session.has_key('loggedin') and session['loggedin'] == 1)

def InitSession(session):
    session.update({'id':None, 'username':'', 'groups':'', 'loggedin':0})
    session.save()

def CheckAccess(auth=False, admin=False, onfail=None):
    def decorator(func):
        def CheckAccessProxy(*args, **kw):
            session = web.ctx.environ['beaker.session']
            if session.is_new:
                InitSession(session)
            if auth:
                if not CheckAuth(session):
                    if not onfail is None:
                        return onfail()
            if admin:
                if not session.has_key('username') or not session['username'] == 'admin':
                    if not onfail is None:
                        return onfail()
            if auth:
                kw.update(session=session)
            return func(*args, **kw)
        return CheckAccessProxy
    return decorator

def WebSessionMiddleware(app):
    return SessionMiddleware(app, data_dir=Conf.session_store, type="file")

