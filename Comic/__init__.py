#!/usr/bin/python
###########################################################################
#    Copyright (C) 2007 by Andrew Mahone
#    <andrew.mahone@gmail.com>
#
# Copyright: See COPYING file that comes with this distribution
#
###########################################################################
"""support modules for scraping and viewing webcomics"""
import sys, os.path, yaml, re, sqlobject, socket

__all__ = [
    'DB',
    'Config',
    'Fetch',
    'Magic',
    'NameEnc',
    'Conf',
    'InitDB',
    'InitConf',
    'Session',
]

if hasattr(yaml,'CSafeDumper'):
    Dumper = yaml.CSafeDumper
else:
    Dumper = yaml.SafeDumper

if hasattr(yaml,'CSafeLoader'):
    Loader = yaml.CSafeLoader
else:
    Loader = yaml.SafeLoader

class ComicLoader(Loader): pass

ComicLoader.add_constructor('!re', lambda x,y: re.compile(x.construct_scalar(y)))

if sys.platform[:3].lower() == 'win':
    win = True
    AppDir = os.path.join(os.getenv('APPDATA') or os.getcwd(), 'comic-get')
    SysAppDir = os.path.join(os.getenv('PROGRAMFILES'), 'comic_get')
    SysConfDir = SysAppDir
else:
    win = False
    AppDir = os.path.join(os.getenv('HOME') or os.getcwd(), '.comic_get')
    SysAppDir = os.path.join('/usr/share', 'comic_get')
    SysConfDir = os.path.join('/etc', 'comic_get')

Conf = None

def InitConf():
    global Conf
    if not Conf is None:
        return
    from Comic import Config
    cdb = os.path.abspath(os.path.join(AppDir,'comics.db'))
    if win:
        cdb = cdb.replace('\\','/').replace(':','|',1)
        cdb = 'sqlite:/' + cdb
    else:
        cdb = 'sqlite://' + cdb
    cdb += "?timeout=15"

    DefConfig = Config.Config(
        comic_store=os.path.join(AppDir,'comics'),
        session_store=os.path.join(AppDir,'sessions'),
        comic_db=cdb,
        dl_attempts=5,
        dl_delay=1.0,
        dl_delay_rand=0.2,
        dl_delay_mul=1.8,
        sock_timeout=5,
        threads=5,
    )
    SysConfigFile = os.path.join(SysConfDir, 'config.yml')
    UsrConfigFile = os.path.join(AppDir, 'config.yml')
    if not os.path.isdir(AppDir):
        os.makedirs(AppDir)
    Conf = Config.Config()
    Conf.update(DefConfig)
    Conf.update(Config.Config(SysConfigFile))
    Conf.update(UsrConfigFile)
    Conf._save()
    if not Conf:
        Conf._parent._parent._save(UsrConfigFile)
    if 'sock_timeout' in Conf:
        socket.setdefaulttimeout(Conf.sock_timeout)

def InitDB():
    from Comic import DB
    DB.SetThreadConnection()
    for obj in (
        DB.Comic,
        DB.Issue,
        DB.Image,
        DB.User,
        DB.UserComic,
        ):
            obj.createTable(ifNotExists=True)

InitConf()