#!/usr/bin/python
###########################################################################
#    Copyright (C) 2007 by Andrew Mahone
#    <andrew.mahone@gmail.com>
#
# Copyright: See COPYING file that comes with this distribution
#
###########################################################################
"""configuration loading and saving via YAML files."""

import sys, os.path, yaml, re

if hasattr(yaml,'CSafeDumper'):
    Dumper = yaml.CSafeDumper
else:
    Dumper = yaml.SafeDumper

if hasattr(yaml,'CSafeLoader'):
    Loader = yaml.CSafeLoader
else:
    Loader = yaml.SafeLoader

_reserved = frozenset(dir(dict()) + dir(object()) + ['and', 'as', 'assert',
    'break', 'class', 'continue', 'def', 'del', 'elif', 'else', 'except',
    'exec', 'finally', 'for', 'from', 'global', 'if', 'import' , 'in', 'is',
    'lambda', 'not', 'or', 'pass', 'print', 'raise', 'return', 'try', 'while',
    'with', 'yield', '_parent', '_file', '_save', '_load'])

_valid_re = re.compile('[A-Za-z][_A-Za-z0-9]*$')

class Config(dict):
    '''
        Config() -> new empty Config
        Config(**kwargs) -> new Config initialized with the name=value pairs in
            the keyword argument list.
        Config(mapping) -> new Config initialized from a mapping object's
            (key, value) pairs.
        Config(seq) -> new Config initialized as if via:
            c = Config()
            for k, v in seq:
                d[k] = v
        Config(myConfig) -> new empty Config object with myConfig as parent
        Config(infile) -> new Config initialized with key/value pairs from
            infile, which may be a file-like object or a string representing
            the pathname of the file to open.  infile must contain a YAML
            document representing a mapping.

        The infile option will not fail on any errors reading the file. Keyword
        arguments may be combined with any one of the other options, in which
        case they will be loaded before the data from the other argument. This
        is most likely to be useful with a file argument, to pass default
        values which will be overridden by values in the config file if it is
        read successfully.

        The values stored in Config objects can be set or retrieved as
        attributes or as keys.  Config will raise an exception on attempts to
        overwrite any attributes it defines or inherits, or to assign a value
        to a keyword which would not be a valid attribute name.  This check
        will occur on any attempt to set a key or attribute, as well as on any
        attributes passed to the constructor.  If a file contains an invalid
        key, none of its contents will be merged into the Config object.

        When there is a parent Config object, items not found in the child will
        be fetched from the parent if possible, but _save will only save items
        in the top level. This can be used to automatically fall back to values
        defined in a system config file or default values without those values
        being written to the a user's config file.
    '''

    def __init__(self, *args, **kw):
        dict.__init__(self)
        object.__setattr__(self, '_parent', None)
        object.__setattr__(self, '_file', None)
        if len(args) > 1:
            raise TypeError, 'Config expected at most 1 non-keyword argument, got 2'
        self.update(*args, **kw)

    def _load(self, infile):
        '''
            Attempt to load data from infile, which may be a pathname or any
            file-like object.  This method will fail silently if there is any
            problem reading the file.
        '''
        try:
            if isinstance(infile,basestring):
                object.__setattr__(self,'_file', infile)
                infile = open(infile, 'rb')
            elif hasattr(outfile, 'name'):
                object.__setattr_(self, '_file', infile.name)
            self.update(**yaml.load(infile,Loader=Loader))
        except: pass

    def _save(self, outfile=None):
        '''
            Write data to outfile, which may be a pathname or any file-like
            object. This method does not hide failures, as _load does.
        '''
        if outfile is None:
            outfile = open(self._file, 'wb')
        elif isinstance(outfile, basestring):
            object.__setattr__(self, '_file', outfile)
            outfile = open(outfile, 'wb')
        elif hasattr(outfile, 'name'):
            object.__setattr__(self, '_file', outfile.name)

        outfile.write('# Config BEGIN\n')
        yaml.dump(dict(self), outfile, default_flow_style=False, Dumper=Dumper)
        outfile.write('# Config END\n')

    def update(self, *args, **kw):
        '''
            Merge new keys into Config object. This works exactly as the
            constructor does, overwriting any values already stored by the
            Config object.
        '''
        if len(args) > 1:
            raise TypeError, 'update expected at most 1 non-keyword argument, got 2'
        for k in kw:
            if k in _reserved or not _valid_re.match(k):
                raise AttributeError, k
        for arg in args:
            if isinstance(arg,basestring) or hasattr(arg, 'read'):
                self._load(arg)
            elif isinstance(arg,Config):
                if object.__getattribute__(self, '_parent'):
                    object.__setattr__(arg, '_parent', self._parent)
                    object.__setattr__(self, '_parent', arg)
                else:
                    object.__setattr__(self, '_parent', arg)
            else:
                self.update(**dict(arg))
        dict.update(self,**kw)

    def __getattribute__(self, key):
        try:
            return object.__getattribute__(self, key)
        except: pass
        try:
            return self[key]
        except: pass
        raise KeyError, key

    def __getitem__(self, key):
        d = self
        while d is not None:
            if dict.has_key(d, key):
                return dict.__getitem__(d, key)
            d = object.__getattribute__(d, '_parent')
        raise KeyError, key

    def __delattr__(self, key):
        try:
            del self[key]
        except KeyError, k:
            raise AttributeError, k

    def __setattr__(self, key, val):
        if key in _reserved or not _valid_re.match(key):
            raise AttributeError, key
        else:
            self[key] = val

    def __str__(self):
        return ''.join(('# Config BEGIN\n',yaml.dump(dict(self), default_flow_style=False, Dumper=Dumper),'# Config END'))

    def __setitem__(self, key, val):
        if key in _reserved or not _valid_re.match(key):
            raise KeyError, key
        else:
            dict.__setitem__(self, key, val)

    def has_key(self, key, descend=True):
        obj = self
        while not obj is None:
            if dict.has_key(obj,key):
                return True
            obj = descend and obj._parent
        return False

    def __contains__(self, key):
        return self.has_key(key)

    def get(self, key, default=None):
        try:
            return self[key]
        except KeyError:
            return default