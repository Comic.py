#!/usr/bin/python
###########################################################################
#    Copyright (C) 2007 by Andrew Mahone
#    <andrew.mahone@gmail.com>
#
# Copyright: See COPYING file that comes with this distribution
#
###########################################################################
"""compact, filename-and-URI encoding of printable text"""

import re, base64

_enc_re = re.compile(r"([A-Za-z0-9() ]+)|([^A-Za-z0-9() ]+)")
_dec_re = re.compile(r"([^.-]+)|(\.)|(-)")
_b66_alphabet = \
    tuple("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_.!")
_b66_reverse = dict(zip(_b66_alphabet, range(66)))
_ascii_chars = re.sub(r"[A-Za-z0-9() ]", '', ''.join(map(unichr, range(33, 127))))
_ascii_tbl = dict(zip(map(ord,_ascii_chars), range(len(_ascii_chars))))
_ascii_rev = dict(zip(range(len(_ascii_chars)), map(ord, _ascii_chars)))
_ascii_off = 160 - len(_ascii_chars)

def _packnums(nums):
    '''
    Pack arbitrary numbers as sequence of values 0-65
    '''
    dat = []
    for num in reversed(nums):
        dat.append(num % 33 + 33)
        num /= 33
        while num:
            dat.append(num % 33)
            num /= 33
    return reversed(dat)

def _unpacknums(dat):
    '''
    Unpack values packed by _packnums
    '''
    Intern
    dat = map(lambda x: _b66_reverse[x], dat)
    ret = []
    num = 0
    for n in dat:
        num = (num * 33) + (n % 33)
        if n > 32:
            ret.append(num)
            num = 0
    return ret

def _mapchar(num):
    '''
    Remap lower characters to allow for shorter encoding of some common input
    '''
    if num in _ascii_tbl:
        return _ascii_tbl[num]
    elif num < 160:
        raise ValueError, 'Illegal character mapping'
    else:
        return num - _ascii_off

def _unmapchar(num):
    '''
    Reverse the mapping performed by _mapchar
    '''
    if num in _ascii_rev:
        return _ascii_rev[num]
    else:
        return num + _ascii_off

def _packchars(chars):
    '''
    Pack string using _mapchar and _packnums
    '''
    return _packnums(map(lambda x: _mapchar(ord(x)), chars))

def _unpackchars(nums):
    '''
    Unpack string packed by _packchars
    '''
    print dat
    return ''.join(map(lambda x: unichr(_unmapchar(x)), nums))

def Encode(text):
    '''
    Returns the encoded form of the input text, which should be a unicode
    string free of control characters.
    '''
    outplain = []
    outenc = []
    outlen = []
    for m in _enc_re.finditer(text):
        g = m.groups()
        if g[0]:
            outplain.append(g[0])
        else:
            sp = m.span()
            l = sp[1] - sp[0]
            if l == 1:
                outplain.append(".")
                outenc.append(g[1])
            else:
                outplain.append("-")
                outlen.append(l-2)
                outenc.append(g[1])
    if outenc:
        outplain.append('!')
        if outlen:
            outlen = _packnums(outlen)
            print outlen
            outplain.extend( map(lambda x: _b66_alphabet[x], outlen))
        if outenc:
            print outenc
            outenc = _packchars(''.join(outenc))
            outplain.extend( map(lambda x: _b66_alphabet[x], outenc))
    return ''.join(outplain).encode('ascii').replace(' ','_')

def Decode(enctext):
    '''
    Converts the encoded text back to its original form.  Always returns unicode.
    '''
    elements = enctext.split('!',1)
    elements[0] = elements[0].replace('_',' ')
    retstr = elements[0].decode('ascii')
    if not elements[1:]:
        return retstr
    numlens = retstr.count("-")
    nums = _unpacknums(elements[1])
    lens = nums[:numlens]
    chrs = _unpackchars(nums[numlens:])
    print chrs
    ret = []
    chrpos = 0
    lenpos = 0
    for m in _dec_re.finditer(retstr):
        g = m.groups()
        if g[0]:
            ret.append(g[0])
        elif g[1]:
            ret.append(chrs[chrpos:chrpos+1])
            chrpos += 1
        else:
            ret.append(chrs[chrpos:chrpos+2+lens[lenpos]])
            chrpos += 2 + lens[lenpos]
            lenpos += 1
    return ''.join(ret)