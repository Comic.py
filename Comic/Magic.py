#!/usr/bin/python
###########################################################################
#    Copyright (C) 2007 by Andrew Mahone
#    <andrew.mahone@gmail.com>
#
# Copyright: See COPYING file that comes with this distribution
#
###########################################################################
"""simple MIME type identification by file contents"""

try:
    import magic
    _magic_cookie = magic.open(magic.MAGIC_PRESERVE_ATIME|magic.MAGIC_MIME|magic.MAGIC_ERROR)
    assert _magic_cookie.load() == 0
    def Path(path):
        return _magic_cookie.file(path)
    def Data(buf):
        return _magic_cookie.buffer(buf)
except:
    def Path(pathname):
        f = file(pathname,'rb')
        d = f.read(4)
        return Data(d)

    def Data(buf):
        if buf[:2] == '\xff\xd8':
            return 'image/jpeg'
        elif buf[:4] == '\x89PNG':
            return 'image/png'
        elif buf[:3] == 'GIF':
            return 'image/gif'
        else:
            return None

Path.__doc__ = '''
        Returns the MIME type of the file found at the given path, or None if
        unable to identify file
'''
Data.__doc__ = '''
        Returns the MIME type of the the data passed to it, or None if unable
        to identify the data
'''