#!/usr/bin/python
###########################################################################
#    Copyright (C) 2007 by Andrew Mahone
#    <andrew.mahone@gmail.com>
#
# Copyright: See COPYING file that comes with this distribution
#
###########################################################################
"""Simple interface for retrieving comics"""
import time, urllib, urllib2, random, yaml, re, BeautifulSoup, \
    copy, urlparse, os.path, NameEnc, sys, mimetypes, threading, traceback
from signal import SIG_IGN, SIGINT, signal
from Comic import DB, Conf, ComicLoader, Magic, InitDB
from htmlentitydefs import name2codepoint
from pkg_resources import resource_stream
HaltLock = threading.Lock()

REType = type(re.compile(''))

Defs = {}
Classes = {}

def checkHalt(func):
    def proxy(*args, **kw):
        if HaltLock.locked():
            raise KeyboardInterrupt
        return func(*args, **kw)
    return proxy

def Merge(infile, theclass, thedict):
    try:
        if isinstance(infile,basestring):
            infile = open(infile, 'rb')
        newdefs = yaml.load(infile, Loader=ComicLoader)
        newdefs = [ (c[0], theclass(name=c[0],**c[1])) for c in \
            newdefs.iteritems() ]
        thedict.update(newdefs)
    except IOError: pass

def FetchURL(url, referer=None, outfile=None):
    '''
        Fetches the requested url, with referer if desired.  If outfile is
        specified, returns url info, otherwise returns (data, url info).
        Exceptions other than KeyboardInterrupt will be ignored silently until
        the specified number of attempts have been made to retrieve the url.
        Fetch will sleep for the specified delay before attempt, with the delay
        increasing exponentially.
    '''
    print url
    attempts = Conf.dl_attempts
    delay = Conf.dl_delay
    while attempts:
        attempts -= 1
        time.sleep(delay * (1 + random.random() * Conf.dl_delay_rand))
        delay *= Conf.dl_delay_mul
        try:
            if isinstance(outfile, basestring):
                outfile = open(outfile, 'wb')
            if outfile:
                b = 0
            else:
                b= ''
            req = urllib2.Request(url)
            if referer:
                req.add_header('Referer',referer)
            handle = urllib2.urlopen(req)
            while True:
                if HaltLock.locked():
                    raise KeyboardInterrupt
                dat = handle.read(4096)
                if len(dat):
                    if outfile:
                        outfile.write(dat)
                        b += len(dat)
                    else:
                        b += dat
                else:
                    if outfile:
                        outfile.close()
                    break;
            i = handle.info()
            if i.has_key('Content-Length'):
                if outfile:
                    n = b
                else:
                    n = len(b)
                if n != int(i['Content-Length']):
                    raise EOFError('Received incomplete file from server')
            if outfile:
                return i
            else:
                return (b, i)
        except KeyboardInterrupt:
            raise
        except:
            if not attempts: raise

_EntityRE = re.compile('(?:&((?:#(\d+)|([A-Za-z]+));)|(["\'&<>]))')

_MSEntities = {
    130: 8218,
    131: 402,
    132: 8222,
    133: 8230,
    134: 8224,
    135: 8225,
    136: 710,
    137: 8240,
    138: 352,
    139: 8249,
    140: 338,
    145: 8216,
    146: 8217,
    147: 8220,
    148: 8221,
    149: 8226,
    150: 8211,
    151: 8212,
    152: 732,
    153: 8482,
    154: 353,
    155: 353,
    156: 339,
    159: 376,
}

_XMLEntities = {
    'quot': '&#34;',
    'amp': '&amp;',
    'apos': '&#39;',
    'lt': '&lt;',
    'gt': '&gt;',
    34: '&#34;',
    38: '&amp;',
    39: '&#39;',
    60: '&lt;',
    62: '&gt;',
    '"': '&#34;',
    '&': '&amp;',
    "'": '&#39;',
    '<': '&lt;',
    '>': '&gt;',
}

def ParseEntities(input,OutputXML=True):
    output = []
    last = 0
    for m in re.finditer(_EntityRE, input):
        if m.start() > last:
            output.append(input[last:m.start()])
        last = m.end()
        ch = ''
        if m.group(2):
            ch = int(m.group(2))
            if ch in _XMLEntities and OutputXML:
                output.append(_XMLEntities[ch])
                continue
            elif ch in _MSEntities:
                ch = _MSEntities[ch]
            try:
                output.append(unichr(ch))
            except:
                continue
        elif m.group(3):
            if m.group(3) in _XMLEntities and OutputXML:
                output.append(_XMLEntities[ch])
                continue
            if m.group(3) in name2codepoint:
                ch = name2codepoint[m.group(3)]
            if ch:
                try:
                    output.append(unichr(ch))
                except:
                    continue
            else:
                if OutputXML:
                    output.append('&amp;')
                    if m.group(1):
                        output.append(m.group(1))
                else:
                    output.append(m.group(0))
        elif m.group(4):
            if OutputXML:
                output.append(_XMLEntities[m.group(4)])
            else:
                output.append(m.group(4))
    output.append(input[last:])
    return ''.join(output)

_DefaultFileRE = re.compile('(?i)comics?/')
_DefaultAnchorURLMap = ('attrs', 'href')
_DefaultImageURLMap = ('attrs', 'src')
_DefaultPrevious = [
    {
        'name': 'a',
        'contents': re.compile('(?i)previous'),
        'index': 0,
        'attrs': {
            'target': False
        },
        'map': {
            'url': _DefaultAnchorURLMap
        }
    },
    {
        'name': 'a',
        'contents': re.compile('(?i)prev'),
        'index': 0,
        'map': {
            'url': _DefaultAnchorURLMap
        }
    },
    {
        'name': 'a',
        'contents': re.compile('(?i)back'),
        'index': 0,
        'map': {
            'url': _DefaultAnchorURLMap
        }
    },
]
_DefaultImage = {
    'name': 'img',
    'attrs': {
        'src': re.compile('(?i)(?:^|/)comics?/')
    },
    'map': {
        'url': _DefaultImageURLMap
    }
}

def TextFullMatch(item, text):
    if isinstance(item, basestring):
        return item == text
    elif isinstance(item, REType):
        return not item.search(text) is None
    else:
        return text in item

def TextSubMatch(item, text):
    if isinstance(item, basestring): return item in text
    if isinstance(item, (list, tuple)):
        for i in item:
            if i in text: return True
        return False
    else:
        return item.search(text)

def ItemMatch(item, tag):
    if 'name' in item and not TextFullMatch(item['name'], tag.name):
        return False
    if 'text' in item and not TextSubMatch(item['text'], tag):
        return False
    if 'contents' in item and not TextSubMatch(item['contents'],
        tag.renderContents(encoding=None)):
        return False
    if 'attrs' in item and item['attrs']:
        attrs = dict(tag.attrs)
        for key, value in item['attrs'].iteritems():
            if value is False:
                if key in attrs:
                    return False
                else:
                    continue
            if not key in attrs:
                return False
            elif value is True:
                continue
            elif not TextFullMatch(value, attrs[key]):
                return False
    for rel, attr in (('next', 'nextSibling'), ('prev', 'previousSibling')):
        if rel in item and item[rel]:
            while True:
                next = getattr(tag,attr)
                if not next: return False
                if isinstance(next, BeautifulSoup.Tag): break
            if not ItemMatch(item[rel],next): return False
    if item.has_key('parent') and item['parent']:
        if not tag.parent:
            return False
        if not ItemMatch(item['parent'],tag.parent): return False
    return True

def DeepMerge(d1, d2):
    for k in set(d1.keys() + d2.keys()):
        if k in d1:
            if k in d2:
                if isinstance(d2[k], dict):
                    d1[k] = DeepMerge(d1[k],d2[k])
        else:
            if isinstance(d2[k], dict):
                d1[k] = DeepMerge(dict(), d2[k])
            else:
                d1[k] = d2[k]

class ETuple(tuple): pass

class ComicClass:
    def __init__(self,**kw):
        if not 'name' in kw:
            raise TypeError("'name' argument is required")
        self.name = kw['name']
        for n in ('archive', 'current', 'previous', 'image'):
            if n in kw:
                setattr(self, n, kw[n])
            else:
                setattr(self, n, {})

class ComicDef:
    def __init__(self,**kw):
        DB.SetThreadConnection()
        if not 'name' in kw:
            raise TypeError("'name' argument is required")
        self.name = kw['name']
        if 'class' in kw:
            if kw['class'] in Classes:
                self.parentclass = Classes[kw['class']]
            else:
                raise TypeError('comic inherits from undefined class "%s"' % kw['class'])
        else:
            self.parentclass=None
        for n in ('url_start', 'url_current', 'url_archive', 'url_stop'):
            setattr(self, n, kw.get(n,''))
        for n in ('archive', 'current', 'previous', 'image'):
            if self.parentclass and getattr(self.parentclass, n):
                classattr = getattr(self.parentclass, n)
                myattr = []
                if n in kw:
                    defattr = kw[n]
                    if isinstance(defattr,dict):
                        defattr = (defattr,)
                    for n in defattr:
                        myattr.append(DeepMerge(n, classattr))
                else:
                    myattr = (classattr),
            else:
                if n in kw:
                    if isinstance(kw[n], dict):
                        myattr = (kw[n],)
                    else:
                        myattr = kw[n]
                else:
                    myattr = ()
            setattr(self,n, ETuple(myattr))
        if not(self.previous or self.archive):
            self.previous = ETuple(_DefaultPrevious)
        if not self.image:
            self.image = ETuple((_DefaultImage,))
        for i in self.previous + self.current + self.archive:
            if 'name' not in i: i['name'] = 'a'
            if 'map' not in i: i['map'] = {'url': _DefaultAnchorURLMap}
            if 'url' not in i['map']: i['map']['url'] = _DefaultAnchorURLMap
        for i in self.image:
            if 'name' not in i: i['name'] = 'img'
            if 'map' not in i: i['map'] = {'url': _DefaultImageURLMap}
            if 'url' not in i['map']: i['map']['url'] = _DefaultImageURLMap
        for match in ('image', 'current', 'archive', 'previous'):
            getattr(self, match).tags = self.GetTags(getattr(self, match))
        if self.previous and self.image:
            if self.previous.tags and self.image.tags:
                tags = self.previous.tags | self.image.tags
            else:
                tags = set()
            self.previous.tags = tags
            self.image.tags = tags
        self.WorkingURL = ''
        if 'massage' in kw:
            self.Massage = copy.copy(BeautifulSoup.BeautifulSoup.MARKUP_MASSAGE)
            self.Massage.extend([ (x, lambda m: m.expand(y)) for x,y in kw['massage'] ])
        else:
            self.Massage = BeautifulSoup.BeautifulSoup.MARKUP_MASSAGE
        try:
            self.DBComic = DB.Comic.byName(self.name)
        except:
            self.DBComic = None
        self.CurrentTitle = ''
        self.DirName = NameEnc.Encode(self.name)
        self.StoreDir = os.path.join(Conf.comic_store, self.DirName)

    @checkHalt
    def Load(self, url, tags=()):
        (dat, inf) = FetchURL(url, referer=self.WorkingURL)
        self.WorkingURL = url
        self.HTMLText = dat
        kw = {}
        if self.Massage:
            kw['markupMassage'] = self.Massage
        if inf.has_key('Content-Type'):
            m = re.search("charset=(.+?)(?:[; ]|$)", inf['Content-Type'])
            if m:
                kw['fromEncoding'] = m.group(1)
        self.HTMLTree = BeautifulSoup.BeautifulSoup(dat, **kw)

    @checkHalt
    def FetchFile(self, url, outfile):
        return FetchURL(url, referer=self.WorkingURL, outfile=outfile)

    def CreateDB(self, clear=0):
        if not self.DBComic:
            self.DBComic = DB.DoInTransaction(DB.Comic, name=self.name)

    def MapItem(self, tag, imap):
        ret = {}
        for k, vm in imap.iteritems():
            v = u''
            mytag = tag
            while vm and vm[0] in ('nextSibling','previousSibling','parent','next','previous','contentsindex','childbyname'):
                if vm[0] == 'contentsindex':
                    mytag = mytag.contents[vm[1]]
                    vm = vm[2:]
                elif vm[0] == 'childbyname':
                    mytag = mytag.find(vm[1])
                    vm = vm[2:]
                else:
                    mytag = getattr(mytag,vm[0])
                    vm = vm[1:]
            if vm[0] == 'attrs':
                if mytag.has_key(vm[1]):
                    v = ParseEntities(mytag[vm[1]])
                    vm = vm[2:]
                else:
                    vm = ()
                    v = ''
            elif vm[0] == 'text':
                v = unicode(mytag)
                vm = vm[1:]
            elif vm[0] == 'contents':
                v = mytag.renderContents(encoding=None)
                vm = vm[1:]
            while vm and v:
                if vm[0] == 'url':
                    v = re.subn('#.*$', '', v)[0]
                    if v:
                        v = urlparse.urljoin(self.WorkingURL, v)
                        vm = vm[1:]
                elif vm[0] == 'urlunquote':
                    v = urllib.unquote(v)
                    vm = vm[1:]
                elif isinstance(vm[0], REType):
                    if vm[1] == 'match':
                        m = vm[0].search(v)
                        if m:
                            v = m.group(0)
                            vm = vm[2:]
                        else:
                            v = ''
                            vm = ()
                    elif vm[1] == 'expand':
                        m = vm[0].search(v)
                        if m:
                            v = m.expand(vm[2])
                        else:
                            v = ''
                            vm = vm[3:]
                    elif vm[1] == 'replace':
                        v = vm[0].sub(vm[2],v)
                        vm = vm[3:]
                else:
                    v = ''
                    vm = ()
            if v:
                v = ' '.join(v.strip().split())
            if k == 'url':
                v = re.subn('#.*$', '', v)[0]
                if v:
                    v = urlparse.urljoin(self.WorkingURL, v)
            if v:
                ret[k] = v
        return ret

    def GetTags(self, item):
        if isinstance(item, (tuple, list)):
            tags = []
            for i in item:
                t = self.GetTags(i)
                if not t:
                    return ()
                else:
                    tags.extend(t)
            return set(tags)
        if 're' in item or not 'name' in item:
            return ()
        tags = [item['name']]
        for rel in ('prev', 'next', 'parent'):
            if rel in item and item[rel]:
                t = self.GetTags(item[rel])
                if not t: return ()
                else: tags.extend(t)
        return set(tags)

    @checkHalt
    def FindItems(self, item):
        if isinstance(item, (tuple, list)):
            for i in item:
                ret = self.FindItems(i)
                if ret: return ret
            return []
        elif 're' in item:
            ret = re.findall(item['re'], unicode(self.HTMLData))
        else:
            ret = self.HTMLTree.findAll(lambda x: ItemMatch(item, x))
        ind = None
        if 'index' in item:
            if item['index'] == 'first':
                ind = 0
            elif item['index'] == 'last':
                ind = -1
            elif not item['index'] is None:
                ind = int(item['index'])
        elif item.has_key('reverse'):
            ret.reverse()
        if not ind is None:
            try:
                ret = [ret[ind]]
            except IndexError:
                return []
        ret = [ self.MapItem(i, item['map']) for i in ret ]
        return ret

    def FindItem(self, item):
        for i in item:
            ret = self.FindItems(i)
            if ret:
                return ret[0]
        return {}

    def FindImages(self):
        ret = self.FindItems(self.image)
        for img in ret:
            if not 'file' in img:
                l = _DefaultFileRE.split(img['url'])
                if len(l) == 2:
                    img['file'] = l[1].replace('/','_')
                else:
                    img['file'] = img['url'].rsplit('/',1)[-1]
        return tuple(ret)

    @checkHalt
    def FetchComic(self, mode='backlog', exists=''):
        item = dict(
            url=self.WorkingURL,
            images=self.FindImages(),
        )
        if self.CurrentTitle:
            item['title'] = self.CurrentTitle
        try:
            for image in item['images']:
                sel = DB.Image.selectBy(
                    comicID=self.DBComic.id,
                    file=image['file']
                )
                if sel.count() == 1:
                    prevdb = sel[0]
                    prev = dict([ (x, getattr(prevdb,x)) for x in
                        ['title', 'extra', 'url', 'file', 'mime']])
                else:
                    prevdb = None
                    prev = {}
                if prevdb:
                    if mode == 'backlog':
                        def fun():
                            prevdb.issue.url = self.WorkingURL
                        DB.DoInTransaction(fun)
                        return None
                filename = os.path.join(self.StoreDir, image['file'])
                m = None
                if not os.path.exists(filename) or exists == 'refetch':
                    headers = self.FetchFile(image['url'], filename)
                    if headers.has_key('Content-Type'):
                        m = headers['Content-Type']
                if not m:
                    m = mimetypes.guess_type(filename)[0]
                if not m:
                    m = Magic.Path(filename)
                if not m:
                    m = ''
                image['mime'] = m
        except:
            for image in item['images']:
                filename = os.path.join(self.StoreDir, image['file'])
                if os.path.exists(filename):
                    try: os.remove(filename)
                    except: pass
            raise
        return item

    @checkHalt
    def GetPrev(self):
        prev = self.FindItem(self.previous)
        url = prev.get('url','')
        if not url:
            return False
        if url in [ getattr(self,x) for x in \
            ('url_current', 'url_start', 'url_archive', \
            'WorkingURL') ]:
            return False
        if DB.Issue.selectBy(
            comicID=self.DBComic.id,
            url=url
        ).count():
            return False
        if url == self.url_stop:
            return False
        self.Load(url, self.image.tags)
        return True

    def FetchComics(self, mode='backlog', exists=''):
        try:
            if not self.DBComic.issues.count():
                mode = 'archive'
            if self.url_archive:
                self.Load(self.url_archive, self.archive.tags)
                archive_comics = self.FindItems(self.archive)
                if mode == 'resume':
                    url = self.DBComic.issues.limit(1)[0].url
                elif mode == 'backlog':
                    url = self.DBComic.issues.reversed().limit(1)[0].url
                else:
                    url = ''
                if url:
                    for n in range(len(archive_comics)):
                        if archive_comics[n]['url'] == url:
                            if mode == 'resume':
                                archive_comics = archive_comics[n+1:]
                            else:
                                archive_comics = archive_comics[:n]
                            break
            else:
                if mode == 'resume':
                    self.Load(self.DBComic.issues.limit(1)[0].url, self.previous.tags)
                    if not self.GetPrev(): return
                else:
                    if self.url_start:
                        self.Load(self.url_start,self.current.tags)
                        cur = self.FindItem(self.current)
                        url = cur.get('url','')
                        if url and url not in [ getattr(self,x) for x in \
                            ('url_current', 'url_start', 'url_archive', \
                            'WorkingURL') ]:
                            self.Load(url, self.previous.tags)
                            self.CurrentTitle = cur.get('title','')
                        else: return
                    else:
                        self.Load(self.url_current,self.previous.tags)
            if not os.path.isdir(self.StoreDir):
                os.makedirs(self.StoreDir)
            if mode == 'archive':
                def fun():
                    self.DBComic.UpdateTimeStamp()
                    DB.Image.deleteBy(comicID=self.DBComic.id)
                    DB.Issue.deleteBy(comicID=self.DBComic.id)
                DB.DoInTransaction(fun)
        except:
                e = sys.exc_info()
                return ("WARNING", "exception raised (%s) while initializing, " \
                    "suggest repeating operation %s later or after resolving " \
                    "the problem" % (e[1].__class__.__name__, mode), e)
        try:
            if mode == 'backlog':
                new_comics = []
            if self.url_archive:
                for comic in archive_comics:
                    self.Load(comic['url'], self.image.tags)
                    self.CurrentTitle = comic.get('title','')
                    iss = self.FetchComic(mode, exists)
                    if not iss:
                        break
                    if mode == 'backlog':
                        new_comics.append(iss)
                    else:
                        DB.DoInTransaction(self.DBFromItem, iss, 'old')
            else:
                while True:
                    iss = self.FetchComic(mode, exists)
                    if not iss:
                        break
                    if mode == 'backlog':
                        new_comics.append(iss)
                    else:
                        DB.DoInTransaction(self.DBFromItem, iss, 'old')
                    if not self.GetPrev(): break
        except:
            e = sys.exc_info()
            if isinstance(e[1], KeyboardInterrupt):
                msg = "keyboard interrupt received"
            else:
                msg = "exception raised (%s)" % e[1].__class__.__name__
            if mode == 'backlog':
                return ("WARNING", "%s, suggest repeating operation backlog" \
                    % msg, e)
            else:
                return ("WARNING", "%s, suggest repeating operation resume" \
                    % msg, e)
        if mode == 'backlog':
            try:
                try:
                    oldhandler = signal(SIGINT, SIG_IGN)
                except: pass
                for iss in reversed(new_comics):
                    DB.DoInTransaction(self.DBFromItem, iss)

            except:
                e = sys.exc_info()
                return("ERROR", "exception raised (%s) during DB update, " \
                    "database may be inconsistent" % e[1].__class__.__name__, \
                    e)
        return None

    def DBFromItem(self, item, ext='new'):
        if self.DBComic.issues.count():
            if ext == 'new':
                serial = self.DBComic.issues.reversed().limit(1)[0].serial + 1
            else:
                serial = self.DBComic.issues.limit(1)[0].serial - 1
        else:
            serial = 0
        if len(item['images']) == 1 and 'title' in item['images'][0] and \
            (item['images'][0]['title'] == item.get('title') \
            or not item.get('title')):
            item['title'] = item['images'][0]['title']
            item['images'][0]['title'] = ''
        iss = DB.Issue(
            comic=self.DBComic,
            title=item.get('title',''),
            serial=serial,
            url=item['url'],
        )
        imgserial = -1
        for i in item['images']:
            imgserial += 1
            img = DB.Image(
                comic=self.DBComic,
                issue=iss,
                title=i.get('title',''),
                extra=i.get('extra',''),
                serial=imgserial,
                file=i['file'],
                mime=i['mime'],
                url=i['url']
            )

def Init():
    classes_source = resource_stream('Comic', 'data/comics/classes.yml')
    definitions_source = resource_stream('Comic', 'data/comics/definitions.yml')
    Merge(classes_source, ComicClass, Classes)
    Merge(definitions_source, ComicDef, Defs)

usage = \
"""Usage: %s [[operation [comics]] | [[comic_options] [comics]]]
Operations:
	-h, --help	Display this usage message
	-l, --list	Lists user's selected comics.
	-L, --list-all	Lists all available comics.
	-A, --add	Add comics to user's selected comics.
	-R, --remove	Remove comics from from user's selected comic.

If none of the above operations are specified, program will run in fetch
backlog mode.

Comic options:
	-a, --archive	The following comics will be fetched in archive mode.
	-r, --resume	The following comics will be fetched in resume mode.
	-b, --backlog	The following comics will be fetched in backlog mode.
	-f, --refetch	Files existing in the comic archive will be re-fetched.
	-F, --no-refetch Files existing in the comic archive will not be
			re-fetched.

Archive mode will fetch from the current strip back to the earliest available
strip. If an archive exists for the comic, its comic strip list will be replaced
with the list of new comics downloaded.

Resume mode will fetch from the oldest strip in the archive's comic strip list
back to the earliest available strip. The new strips will be appended to the
existing list.

Backlog mode will fetch from the current strip back to the newest strip already
in the archive's comic strip list. The new strips will be prepended to the
existing list upon success.

If no comics are specified, all comics selected by the user will be fetched in
the specified mode."""

def FetchThreaded(comic, mode, exists):
    DB.SetThreadConnection()
    ret = comic.FetchComics(mode=mode, exists=exists)
    if not ret is None:
        comlock.acquire()
        errs.append((comic,)+ret)
        comlock.release()
    exitsignal.set()

def Run():
    global comlock, errs, exitsignal
    InitDB()
    Init()
    from Comic import Conf
    comlock = threading.Lock()
    exitsignal = threading.Event()
    mode = 'backlog'
    exists = ''
    errors = []
    comiclist = []
    err = False
    opp = 'fetch'
    AppName = os.path.basename(sys.argv[0])
    if '-h' in sys.argv or '--help' in sys.argv[1:]:
        print usage % sys.argv[0]
        sys.exit()
    if len(sys.argv) > 1:
        if sys.argv[1] in ('--list','-l','--list-all','-L'):
            if len(sys.argv) > 2:
                sys.exit("--list and --list-all are not valid with other options. Run \"%s --help\" for usage."%(sys.argv[0],))
            if sys.argv[1] in ('--list','-l'):
                comics = sorted(map(lambda x: x.name,
                    DB.Comic.select()), lambda x,y:
                    cmp(x.lower(), y.lower()))
                if not comics:
                    sys.stderr.write("No comics in user comic list.\n")
                    sys.exit()
            else:
                comics = Defs.keys()
                comics.sort()
            for c in comics:
                print c
            sys.exit()
        elif sys.argv[1] in ('-A','--add','-R','--remove'):
            comiclist = []
            if sys.argv[1] in ('-A','--add'):
                for c in sys.argv[2:]:
                    if not c in Defs:
                        sys.exit("The comic \"%s\" does not exist in the set of available comic definitions."%c)
                    elif not c in comiclist:
                        comiclist.append(c)
                for c in comiclist:
                    Defs[c].CreateDB()
            else:
                for c in sys.argv[2:]:
                    if c in Defs:
                        sys.exit("The comic \"%s\" does not exist in the set of available comic definitions."%c)
                    elif not c in comiclist:
                        comiclist.append(c)
                for c in comiclist:
                    DB.Comic.deleteBy(name=c)
            sys.exit()
    fetchlist = []
    for arg in sys.argv[1:]:
        if arg in ('--resume','-r'):
            mode = 'resume'
        elif arg in ('--archive','-a'):
            mode = 'archive'
        elif arg in ('--backlog','-b'):
            mode = 'backlog'
        elif arg in ('--refetch','-f'):
            exists = 'refetch'
        elif arg in ('--no-refetch','-F'):
            exists = ''
        else:
            if arg in Defs:
                if Defs[arg].DBComic:
                    fetchlist.append((Defs[arg],mode,exists))
                else:
                    sys.exit("The comic \"%s\" is not selected for fetching. Try \"%s -A '%s'\" to add it."%(arg,AppName,arg))
            else:
                sys.exit("The comic \"%s\". does not exist. Try \"%s -L\" to list available comics."%(arg,AppName))
    if not fetchlist:
        s = DB.Comic.select()
        if s.count():
            fetchlist = map(lambda x: (Defs[x.name],mode,exists), s)
        else:
            sys.exit("No comics specified and none in user's set of selected comics. Try \"%s -h\" to see more options."%AppName)
    errs = []
    if Conf.threads:
        try:
            for i in fetchlist:
                while len(threading.enumerate()) > Conf.threads:
                    exitsignal.wait(1)
                exitsignal.clear()
                t = threading.Thread(target=FetchThreaded, name=i[0].name, args=i)
                t.start()
        except KeyboardInterrupt:
            HaltLock.acquire(False)
        while len(threading.enumerate()) > 1:
            try:
                while len(threading.enumerate()) > 1:
                    exitsignal.wait(1)
                    exitsignal.clear()
            except KeyboardInterrupt:
                HaltLock.acquire(False)
    else:
        for c in fetchlist:
            ret = c[0].FetchComics(c[1],c[2])
            if not ret is None:
                errs.append((c[0],) + ret)
    for e in errs:
        print "%s: (%s) %s" % (e[1],e[0].name,e[2])
        traceback.print_exception(e[3][0],e[3][1],e[3][2])