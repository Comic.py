var lastHash;
var currentComic;
var newComic;
var newIssue;
var currentIssue;
var initDone;
var myComics;
var myIssues;
var myImages;
var myComicsTime = '0';
var myIssuesTime = '0';
var prevTargetIssue;
var prevTargetComic;
var prevSelIssue;
var comicTemplate;
var issueTemplate;
var issueScrolling;
var marks = new Array;

function getUniqueChild(node, name)
{
	var nodeList = node.getElementsByTagName(name);
	if (nodeList || nodeList.length == 1)
		return nodeList[0]
	else
		return null;
}

function updateHash(newHash, replaceHistory)
{
	pollHash.stop()
	var newURL = window.location.href.replace(/$|(#.*)$/,newHash.replace(/^#?/,'#'));
	if (replaceHistory && window.location.replace)
		window.location.replace(newURL);
	else
		window.location.href = newURL;
	lastHash = newHash.replace(/^#?/,'');
	pollHash.start();
}

function scrollSelIssue(issue)
{
	var targetIssue;
	if(targetIssue = $(currentComic + '#' + issue))
	{
		if (targetIssue.addClass)
			targetIssue.addClass("current");
		if (prevSelIssue && prevSelIssue.hasClass && prevSelIssue.hasClass('selected'))
			prevSelIssue.removeClass('selected');
		prevTargetIssue = targetIssue;
		myIssues.scrollTop = targetIssue.offsetTop - 30;
	};
};

function markItem(item,className,scrollIt,where)
{
	var itemCoords;
	var parentCoords;
	var newTop;
	var targetItem;
	var parentItem;
	var prevItem;
	var containerId;
	if(targetItem = $(item))
	{
		if(targetItem.addClass)
			targetItem.addClass(className);
		if(parentItem = targetItem.getParent())
		{
			if(containerId = parentItem.id)
			{
				if(!marks[containerId])
					marks[containerId] = new Array;
				if(prevItem = marks[containerId][className])
					if(prevItem != targetItem)
						prevItem.removeClass(className);
				marks[containerId][className] = targetItem;
			}
			if(scrollIt)
				scrollItem(targetItem,where);
		};
	};
};

function scrollItem(item,where)
{
	var parentItem;
	var newTop;
	
	item = $(item);
	if(!item)
		return;
	parentItem = item.getParent();
	if(!parentItem)
		return;
	
	newTop = item.offsetTop;
	switch(where)
	{
	case 'center':
	case 'middle':
		newTop -= (parentItem.offsetHeight-item.offsetHeight)/2;
		break;
	case 'bottom':
		newTop -= (parentItem.offsetHeight-item.offsetHeight);
	}
	newTop = Math.max(0,newTop);
	parentItem.scrollTop=newTop;	
}

function unmarkItem(id,className)
{
	var targetItem;
	if(marks[id] && marks[id][className] && (targetItem = $(marks[id][className])))
	{
		targetItem.removeClass(className);
		delete marks[id][className];
	}
}

function clicky(event)
{
	if(event && event.target && event.target.nodeName && event.target.nodeName == 'a')
		return;
	var myComponents = this.id.split('#');
	updateState(myComponents[0], myComponents[1]);
}
function xmlUpdate()
{
	properties = []
	var comics;
	var issues;
	var images;
	var nodes;
	var issue;
	var tmp1;
	var tmp2;
	var node;
	var myId;
	var myTitle;
	var myName;
	var comicsTime;
	var issuesTime;
	var newHash = '';
	var i;
	var oldHTML;
	var children;
	if (this.response && this.response.xml && (comics = getUniqueChild(this.response.xml,'comics')))
	{
		if (newComic = comics.getAttribute('c'))
		{
			currentComic = newComic;
			newHash =  currentComic;
		}
		if (comicsTime = comics.getAttribute('h'))
		{
			myComicsTime = comicsTime;
			var newComics = myComics.clone(false);
			nodes = comics.getElementsByTagName('comic');
			for (i = 0; i < nodes.length; i++)
			{
				node = nodes[i];
				myId = node.getAttribute('i');
				myName = node.getAttribute('n');
				newComics.appendChild(comicTemplate.clone(true));
				newComics.lastChild.id = myId;
				newComics.lastChild.firstChild.nodeValue = myName;
				if (node.getAttribute('h'))
				{
					console.log('hiding ' + myName)
					newComics.lastChild.addClass('hidden');
				}
				newComics.lastChild.addEvent('click',clicky);
			}
			myComics = myComics.replaceWith(newComics);
		}
	}
	if (comics && (issues = getUniqueChild(comics,'issues')))
	{
		if (currentIssue = issues.getAttribute('c'))
		{
			newHash += '#' + currentIssue;
		}
		if (issuesTime = issues.getAttribute('t'))
		{
			myIssuesTime = issuesTime;
			var newIssues = myIssues.clone(false);
			nodes = issues.getElementsByTagName('issue');
			for (i = 0 ; i <nodes.length; i++)
			{
				node = nodes[i];
				myId = node.getAttribute('i');
				newIssues.appendChild(issueTemplate.clone(true));
				newIssues.lastChild.id = currentComic + '#' + myId;
				newIssues.lastChild.lastChild.lastChild.nodeValue = i+1;
				if (myTitle = node.getAttribute('t'))
 					newIssues.lastChild.appendText(': ' + myTitle);
				newIssues.lastChild.addEvent('click',clicky);

			}
			myIssues = myIssues.replaceWith(newIssues);
		}
	}
	else
	{
		myIssues.innerHTML = '';
		myIssuesTime = '0'
	}
	if (issues && (images = getUniqueChild(issues,'images')))
	{
		var newImages = imagesTemplate.clone(true);
		myURL = images.parentNode.getAttribute('u');
		myNum = images.parentNode.getAttribute('n');
		newImages.firstChild.firstChild.href = myURL;
		newImages.firstChild.firstChild.lastChild.firstChild.nodeValue = myNum;
		if (myTitle = images.parentNode.getAttribute('t'))
			try
			{
				oldHTML = newImages.firstChild.innerHTML;
				newImages.lastChild.innerHTML += ': ' + myTitle;
			}
			catch (e)
			{
				newImages.firstChild.appendText(myTitle);
			}
		nodes = images.getElementsByTagName('image');
		for (i = 0; i <nodes.length; i++)
		{
			node = nodes[i];
			myTitle = node.getAttribute('t');
			myExtra = node.getAttribute('e');
			myURL = node.getAttribute('u');
			mySRC = 'image?id=' + node.getAttribute('i');
			if(myTitle)
			{
				newImages.appendChild(document.createElement('div'));
				newImages.lastChild.className = 'imagetitle';
				newImages.lastChild.appendChild(document.createTextNode(myTitle));
			}
			if(myExtra)
			{
				newImages.appendChild(document.createElement('div'));
				newImages.lastChild.className = 'imageextra';
				newImages.lastChild.appendChild(document.createTextNode(myExtra));
			}
			newImages.appendChild(imageTemplate.clone(true));
			newImages.lastChild.href = myURL;
			newImages.lastChild.firstChild.src = mySRC;
		}
		myImages = myImages.replaceWith(newImages);
	}
	else
	{
		myImages.innerHTML = '';
	}
	var comicObj = $(currentComic);
	var issueObj = $(currentComic + '#' + currentIssue);
	markItem(comicObj,'current',true,'middle');
	markItem(issueObj,'current',true,'middle');
	unmarkItem('comics','loading');
	unmarkItem('issues','loading');
	unmarkItem('issues','selected');
	issueScrolling = 0;
	if (newHash)
		newHash += '#';
	if (newHash != lastHash)
	{
		updateHash(newHash);
	}
}

function pollHash()
{
	var myHash = document.location.href.replace(/^[^#]*(#|$)/,'');
	if (myHash != lastHash)
	{
		console.log(myHash);
		console.log(lastHash);
		updateStateFromId(myHash);
		lastHash = myHash;
	}
}

pollHash.start = function()
{
	if(!pollHash.intervalHandle)
	{
		pollHash.intervalHandle = setInterval(pollHash,300);
	}
}

pollHash.stop = function()
{
	if(pollHash.intervalHandle )
	{
		clearInterval(pollHash.intervalHandle);
		delete pollHash.intervalHandle;
	}
}

function updateState(newComic, newIssue)
{
	var query = 'comics_ts=' + myComicsTime;
	if(newComic)
	{
		query += '&comic=' + newComic;
		if(newIssue)
		{
			query += '&issue=' + newIssue;
		}
		if (newComic != currentComic)
			myIssuesTime = '0';
		query += '&issues_ts=' + myIssuesTime;
	}
	var myXHR = new XHR({method: 'get'});
	if (!myXHR)
		alert("Unable to create XMLHttpRequest, dynamic view will not work.");
	else
	{
		if(newComic != currentComic)
			markItem(newComic,'loading');
		markItem(newComic + '#' + newIssue,'loading');
		myXHR.addEvent('onSuccess', xmlUpdate);
		myXHR.send('xml',query);
	}
}

function updateStateFromId(myId)
{
	updateState.apply(updateState,myId.split('#'));
}

function updateStateFromUrl()
{
	var myId = window.location.href.replace(/^[^#]*#/,'');
	updateStateFromId(myId);
}

function clearSelection(scrollBack)
{
	issueScrolling = 0;
	unmarkItem('issues','selected');
	if(scrollBack && marks['issues'] && marks['issues']['current'])
		scrollItem(marks['issues']['current'],'middle');
	clearSelection.cancelDelay();
}

clearSelection.cancelDelay = function()
{
	if (clearSelection.delayHandle)
	{
		$clear(clearSelection.delayHandle)
		delete clearSelection.delayHandle;
	}
}

clearSelection.setDelay = function(delay, scrollBack)
{
	if (clearSelection.delayHandle)
		$clear(clearSelection.delayHandle);
	clearSelection.delayHandle = clearSelection.delay(delay,this,scrollBack)
}

function hotKeys(event)
{
	var nextSelect;
	switch(event.type)
	{
	case 'keydown':
		if (event.alt || event.meta || event.control || event.shift)
			return;
		switch(event.key)
		{
		case 'left':
		case 'right':
			if (!issueScrolling)
			{
				issueScrolling = 1;
				markItem(currentComic + '#' + currentIssue, 'selected');
			}
		case 'up':
		case 'down':
			event.stop();
			break;
		}
		break;
	case 'keypress':
		if (event.alt || event.meta || event.control || event.shift)
			return;
		switch(event.key)
		{
		case 'left':
			if (marks['issues'] && marks['issues']['selected'] && (nextSelect = marks['issues']['selected'].getPrevious()))
			{
				markItem(nextSelect,'selected',true,'middle');
			}
			issueScrolling += issueScrolling < 3;
			event.stop();
			break;
		case 'right':
			if (marks['issues'] && marks['issues']['selected'] && (nextSelect = marks['issues']['selected'].getNext()))
			{
				markItem(nextSelect,'selected',true,'middle');
			}
			issueScrolling += issueScrolling < 3;
			event.stop();
			break;
		case 'up':
			myImages.scrollTop -= myImages.clientHeight / 4;
			event.stop();
			break;
		case 'down':
			myImages.scrollTop += myImages.clientHeight / 4;
			event.stop();
			break;
		}
		break;
	case 'keyup':
		switch(event.key)
		{
		case 'left':
			if (!issueScrolling)
			{
				event.stop()
				break;
			}
			if (issueScrolling == 1 && marks['issues'] && marks['issues']['selected'] && (nextSelect = marks['issues']['selected'].getPrevious()))
			{
				markItem(nextSelect,'selected',true,'middle');
				issueScrolling++;
			}
			if (issueScrolling == 2)
			{
				if(marks['issues'] && marks['issues']['selected'])
					clicky.call(marks['issues']['selected']);
				clearSelection();
			}
			else
				clearSelection.setDelay(5000,true)
			event.stop();
			break;
		case 'right':
			if (!issueScrolling)
			{
				event.stop()
				break;;
			}
			if (issueScrolling == 1 && marks['issues'] && marks['issues']['selected'] && (nextSelect = marks['issues']['selected'].getNext()))
			{
				markItem(nextSelect,'selected',true,'middle');
				issueScrolling++;
			}
			if (issueScrolling == 2)
			{
				if(marks['issues'] && marks['issues']['selected'])
					clicky.call(marks['issues']['selected']);
				clearSelection();
			}
			else
				clearSelection.setDelay(5000,true)
			event.stop();
			break;
		case 'up':
		case 'down':
			if (event.alt || event.meta || event.control || event.shift)
				return;
			event.stop();
			break;
		case 'enter':
			if (event.alt || event.meta || event.control || event.shift || !issueScrolling)
				return;
			if(marks['issues'] && marks['issues']['selected'])
				clicky.call(marks['issues']['selected']);
			clearSelection();
		case 'esc':
			if (event.alt || event.meta || event.control || event.shift || !issueScrolling)
				return;
			clearSelection(true);
		}
		break;
	}
}

function dynamicStartup()
{
	var i;
	if (!document.getElementById || !document.getElementsByTagName || !(new XHR({method: 'get'})) || !(new XHR({method: 'post'})))
		return;
	if (document.location.search)
	{
		var newURL = window.location.href.replace(/\?.*/,'');
		var queryElements = window.location.search.replace(/^\?/,'').split('&');
		var queryHash = {};
		for (i = 0; i < queryElements.length; i++)
		{
			var components = queryElements[i].split('=');
			queryHash[unescape(components[0])] = unescape(components[1]);
		}
		if (queryHash['comic']) {
			newURL += '#' + queryHash['comic'];
			if (queryHash['issue']) {
				newURL += '#' + queryHash['issue'];
			}
		}
		newURL += '#';
		if (window.location.replace)
			window.location.replace(newURL);
		else
			window.location.href = newURL;
	}
	links = document.getElementsByTagName('a');
	if (links)
	{
		for (i = 0; i < links.length; i++)
		{
			links[i].href='#';
		}
	}
	myComics = $('comics');
	myIssues = $('issues');
	myImages = $('images');
	comicTemplate = new Element('div').appendText('-');
	issueTemplate = new Element('div').appendText('#').adopt(new Element('span',{className:'fixed'}).appendText('-'));
	imagesTemplate = $(myImages).clone(false);
	imagesTemplate.appendChild(new Element('div'));
	imagesTemplate.firstChild.id = 'issuetitle';
	imagesTemplate.firstChild.appendChild(document.createElement('a'));
	imagesTemplate.firstChild.firstChild.appendChild(document.createTextNode('#'));
	imagesTemplate.firstChild.firstChild.appendChild(document.createElement('span'));
	imagesTemplate.firstChild.firstChild.lastChild.className = 'fixed';
	imagesTemplate.firstChild.firstChild.lastChild.appendChild(document.createTextNode('-'));
	imageTemplate = new Element('a').adopt(new Element('img'));
	var boundHotKeys = hotKeys.bindWithEvent(document);
	$(document).addEvent('keyup',boundHotKeys);
	$(document).addEvent('keypress',boundHotKeys);
	$(document).addEvent('keydown',boundHotKeys);
	lastHash = '';
	pollHash.start();
	updateStateFromUrl();
}
$(window).addEvent('domready',dynamicStartup);