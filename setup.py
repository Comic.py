#!/usr/bin/python
import ez_setup
ez_setup.use_setuptools(version='0.6c6')
from setuptools import setup
import sys, os.path

setup(
    name='Comic',
    version='0.6.dev',
    description='Web Comic Scraper / Viewer',
    author='Andrew Mahone',
    author_email='andrew.mahone@gmail.com',
    packages=['Comic'],
    package_dir={'Comic': 'Comic'},
    package_data={'Comic': [
        'data/templates/*.xml', 
        'data/templates/*.html', 
        'data/static/*.css',
        'data/static/*.js',
        'data/static/*.htc',
        'data/static/*.html',
        'data/static/*.png',
        'data/comics/*.yml',
    ]},
    install_requires='''
        BeautifulSoup >= 3.0
        SQLObject >= 0.9
        PyYAML >= 3.0
    ''',
    extras_require={
        'fetch': ['BeautifulSoup >= 3.0'],
        'webdisplay': ['web.py >= 0.2', 'beaker >= 0.7'],
        'sqlite': ['pysqlite'],
        'mysql': ['MySQL-python'],
        'postgresql': ['psycopg2'],
    },
    entry_points={
        'console_scripts': [
            'comic_show = Comic.Web:run [fetch]',
            'comic_get = Comic.Fetch:Run [webdisplay]',
        ]
    },
)
